
# elham-pakroo2
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html
        PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <form style="margin:5%" method="post" action="form.php">
        <div class="form-group">
            <label for="name">Your Name</label>
            <input type="text" class="form-control" name="name" placeholder="elham" pattern=[A-Z\sa-z]{3,20} required>
        </div>
        <div class="form-group">
            <label for="familyname">Your familyname</label>
            <input type="text" class="form-control" name="familynamename" placeholder="pakroo" pattern=[A-Z\sa-z]{3,20} required>
        </div>
        <div class="form-group">
            <label for="number">Your phone</label>
            <input type="number" class="form-control" name="number" placeholder="09122109865" pattern={3,20} required>
        </div>
        <div class="form-group">
            <label for="email">Your Email</label>
            <input type="email" class="form-control" id="email" placeholder="Enter email" required>
            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        <div class="form-group">
            <label for="title">Your title</label>
            <input type="text" class="form-control" name="title" placeholder="mozo" pattern=[A-Z\sa-z]{3,20} required>
        </div>

        <div class="form-group">
            <label for="message">Write your message</label>
            <textarea class="form-control" id="message" name="visitor_message" placeholder="message"
                      required></textarea>
        </div>
    </form>
        <button type="submit" class="btn btn-primary">Submit</button>

</head>

<body>

</body>

</html>
